package u04lab.code

import Optionals._
import Optionals.Option._
import Lists._
import Lists.List._
import Streams._
import Streams.Stream._
import u04lab.code.PowerIterator.BaseIterator

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

object PowerIterator {

  case class BaseIterator[A](var stream: Stream[A]) extends PowerIterator[A] {
    override var allSoFar: List[A] = Nil()

    override def next() = stream match {
      case Streams.Stream.Cons(head, tail) => {
        stream = tail()
        Some(head())
      }
      case _ => None()
    }

    override def reversed(): PowerIterator[A] = IteratorWrapper(this, reverse(allSoFar))
  }

  case class IteratorWrapper[A](iterator: PowerIterator[A], items: List[A]) extends PowerIterator[A] {
    override var allSoFar: List[A] = items

    override def next() = iterator.next()

    override def reversed(): PowerIterator[A] = IteratorWrapper(this, reverse(allSoFar))
  }
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] =
    BaseIterator(iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] =
    BaseIterator(toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = {
    val rng = new Random()
    BaseIterator(take(generate(rng.nextBoolean()))(size))
  }
}
