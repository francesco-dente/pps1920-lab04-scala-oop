package u04lab.code

trait Complex {
  def re: Double
  def im: Double
  def +(c: Complex): Complex // should implement the sum of two complex numbers..
  def *(c: Complex): Complex // should implement the product of two complex numbers
}

object Complex {
  //def apply(re:Double, im:Double):Complex = new ComplexImpl(re, im)
  def apply(re:Double, im:Double):Complex = CaseComplex(re, im)

  class ComplexImpl(val re: Double, val im: Double) extends Complex {
    override def +(c: Complex) = new ComplexImpl(this.re + c.re, this.im + c.im)
    override def *(c: Complex) = new ComplexImpl(this.re * c.re - this.im * c.im, this.re * c.im + this.im * c.re)
  }

  case class CaseComplex(re: Double, im: Double) extends Complex {
    override def +(c: Complex) = CaseComplex(this.re + c.re, this.im + c.im)
    override def *(c: Complex) = CaseComplex(this.re * c.re - this.im * c.im, this.re * c.im + this.im * c.re)
  }
}

object TryComplex extends App {
  val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
  val c = a(0) + a(1) + a(2)
  println(c, c.re, c.im)
  val c2 = a(0) * a(1)
  println(c2, c2.re, c2.im)
  println(Complex(10,20) == Complex(10,20))
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides
  * - check that equality and toString do not work
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */
